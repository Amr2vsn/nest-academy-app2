import React, { useContext } from 'react';
import { Dimensions, View } from 'react-native';
import { ProgressStepsContext } from './progress-steps';
import { Box, Button, Text, ArrowIcon } from '../';
import { ProgressBar } from '../progress-bar';
import { useNavigation } from '@react-navigation/core';
const { width } = Dimensions.get('window');

export const Header: React.FC<any> = () => {
  const { childrens, currentPageIndex, setCurrentPageIndex } = useContext(
    ProgressStepsContext
  );
  const progressLength = (100 / childrens.length) * (currentPageIndex + 1);

  return (
    // <Box width={width} height={54}>
    <Box width={width} height={5} role={'primary300'}>
      <ProgressBar
        role={'success400'}
        endProgress={progressLength}
        duration={1000}
      />
    </Box>
    // </Box>
  );
};
