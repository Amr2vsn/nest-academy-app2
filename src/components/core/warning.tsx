import React from 'react';
import {
  Box,
  Text,
  Shadow,
  Button,
  Spacing,
  RightArrowIcon,
} from '../../components';
import { Border } from './border';
import { useNavigation } from '@react-navigation/native';

export const Warning: React.FC<WarningType> = ({
  title,
  buttonname,
  twobutton = true,
  children,
  onPress,
  titleAlign = 'center',
}) => {
  const navigation = useNavigation();
  return (
    <Box height={210} width={343}>
      <Shadow role="primary500" opacity={0.25} w={2}>
        <Shadow role="primary500" opacity={0.15} w={2}>
          <Border radius={4}>
            <Box height={189} width={343} role="primary100">
              <Box flex={1} role="primary100" alignItems="center">
                <Spacing mt={6}>
                  {titleAlign === 'center' ? (
                    <Text
                      role="primary500"
                      type="title3"
                      bold={true}
                      textAlign="center"
                    >
                      {title}
                    </Text>
                  ) : (
                    <Box width={280} height={30} justifyContent="flex-start">
                      <Text
                        role="primary500"
                        type="title3"
                        textAlign="left"
                        bold={true}
                      >
                        {title}
                      </Text>
                    </Box>
                  )}
                </Spacing>
                <Spacing mt={4}>{children}</Spacing>
              </Box>
            </Box>
          </Border>
        </Shadow>
      </Shadow>
      <Box width={'100%'} position="absolute" bottom={0}>
        {twobutton ? (
          <Box width="100%" flexDirection="row" justifyContent="space-between">
            <Button
              onPress={() => navigation.goBack()}
              width={167}
              category="ghost"
            >
              <Box
                justifyContent="center"
                alignItems="center"
                role="white"
                width={167}
                height="100%"
              >
                <Text fontFamily="Montserrat" type="callout" bold>
                  ӨМНӨХ
                </Text>
              </Box>
            </Button>
            <Button onPress={onPress} width={167}>
              <Box
                flexDirection="row"
                flex={1}
                justifyContent="center"
                alignItems="center"
              >
                <Text fontFamily="Montserrat" type="callout" role="white" bold>
                  {buttonname.toUpperCase()}
                </Text>
                <Box width={30} alignItems="center">
                  <RightArrowIcon />
                </Box>
              </Box>
            </Button>
          </Box>
        ) : (
          <Button onPress={onPress} category="fill" size="l" width={'100%'}>
            <Box
              flexDirection="row"
              justifyContent="center"
              alignItems="center"
            >
              <Box>
                <Text
                  type="callout"
                  role="white"
                  bold={true}
                  fontFamily="Montserrat"
                >
                  {buttonname.toUpperCase()}
                </Text>
              </Box>
              <Box width={40} justifyContent="center" alignItems="center">
                <RightArrowIcon />
              </Box>
            </Box>
          </Button>
        )}
      </Box>
    </Box>
  );
};

type WarningType = {
  title: string;
  buttonname: string;
  onPress: Function;
  twobutton?: boolean;
  children?: any;
  titleAlign?: 'center' | 'left';
};
