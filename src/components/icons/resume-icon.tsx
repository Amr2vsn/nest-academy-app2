import React from 'react';
import Svg, { Path } from 'react-native-svg';
import { IconType } from '../types';

export const ResumeIcon: React.FC<IconType> = ({ height = 49, width = 49 }) => {
  return (
    <Svg width={width} height={height} viewBox="0 0 38 38" fill="none">
      <Path
        d="M19 34.833c-8.744 0-15.833-7.088-15.833-15.833 0-8.745 7.089-15.834 15.833-15.834 8.745 0 15.834 7.09 15.834 15.834 0 8.745-7.089 15.833-15.834 15.833zm0-3.166a12.666 12.666 0 100-25.333 12.666 12.666 0 000 25.332zM16.82 13.323l7.725 5.149a.633.633 0 010 1.054l-7.727 5.15a.634.634 0 01-.983-.527v-10.3a.632.632 0 01.985-.526z"
        fill="#FAFAFA"
      />
    </Svg>
  );
};
