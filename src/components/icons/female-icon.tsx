import React from 'react';
import Svg, { Path } from 'react-native-svg';
import { useTheme } from '../theme-provider';
import { IconType } from '../types';

export const FemaleIcon: React.FC<IconType> = ({ height = 24, width = 16 }) => {
  const { colors } = useTheme();
  return (
    <Svg width={width} height={height} viewBox="0 0 16 24" fill="none">
      <Path
        d="M6.208 23.408v-3.434H2.434v-2.958h3.774v-2.142a6.943 6.943 0 01-2.958-1.36 8.093 8.093 0 01-2.074-2.55 7.408 7.408 0 01-.748-3.298c0-1.315.34-2.516 1.02-3.604A7.714 7.714 0 014.134 1.41 6.897 6.897 0 017.806.39c1.337 0 2.561.34 3.672 1.02a7.351 7.351 0 012.686 2.652 6.665 6.665 0 011.02 3.604c0 1.156-.25 2.244-.748 3.264a7.52 7.52 0 01-2.074 2.55 6.74 6.74 0 01-2.958 1.394v2.142h3.536v2.958H9.404v3.434H6.208zm1.598-11.39c.793 0 1.519-.193 2.176-.578a4.588 4.588 0 001.564-1.598 4.045 4.045 0 00.612-2.176 3.76 3.76 0 00-.612-2.074 4.311 4.311 0 00-1.564-1.564 4.224 4.224 0 00-2.176-.578c-.793 0-1.519.193-2.176.578a4.69 4.69 0 00-1.564 1.564 3.923 3.923 0 00-.578 2.074c0 .793.193 1.519.578 2.176.408.657.94 1.19 1.598 1.598a4.162 4.162 0 002.142.578z"
        fill="#172B4D"
      />
    </Svg>
  );
};
