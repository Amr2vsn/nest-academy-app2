import * as React from 'react';
import Svg, { Path } from 'react-native-svg';
import { IconType } from '../types';

export const PencilIcon: React.FC<IconType> = ({ width = 16, height = 16 }) => {
  return (
    <Svg width={width} height={height} viewBox="0 0 16 16" fill="none">
      <Path
        d="M11.107 6.072L9.928 4.893l-7.761 7.762v1.178h1.178l7.762-7.761zm1.178-1.179l1.178-1.178-1.178-1.178-1.178 1.178 1.178 1.178zM4.035 15.5H.5v-3.536L11.696.768a.833.833 0 011.178 0l2.358 2.358a.833.833 0 010 1.178L4.036 15.5h-.001z"
        fill="#172B4D"
      />
    </Svg>
  );
};
