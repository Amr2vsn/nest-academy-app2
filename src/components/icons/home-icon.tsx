import * as React from 'react';
import Svg, { Path } from 'react-native-svg';
import { useTheme } from '../theme-provider';
import { IconType } from '../types';

export const HomeIcon: React.FC<IconType> = ({
  role = 'darkgrey',
  width = 24,
  height = 24,
}) => {
  const { colors } = useTheme();
  return (
    <Svg width={width} height={height} viewBox="0 0 24 24" fill="none">
      <Path
        d="M13 19h6V9.978l-7-5.444-7 5.444V19h6v-6h2v6zm8 1a1 1 0 01-1 1H4a1 1 0 01-1-1V9.49a1 1 0 01.386-.79l8-6.222a1 1 0 011.228 0l8 6.222a1 1 0 01.386.79V20z"
        fill={colors[role]}
      />
    </Svg>
  );
};
