import * as React from 'react';
import Svg, { Path } from 'react-native-svg';
import { useTheme } from '../theme-provider';
import { IconType } from '../types';

export const ProfileIcon: React.FC<IconType> = ({
  role = 'black',
  width = 24,
  height = 24,
}) => {
  const { colors } = useTheme();
  return (
    <Svg width={width} height={height} viewBox="0 0 25 24" fill="none">
      <Path
        d="M20.25 22h-2v-2a3 3 0 00-3-3h-6a3 3 0 00-3 3v2h-2v-2a5 5 0 015-5h6a5 5 0 015 5v2zm-8-9a6 6 0 110-12.002 6 6 0 010 12.002zm0-2a4 4 0 100-8 4 4 0 000 8z"
        fill={colors[role]}
      />
    </Svg>
  );
};
