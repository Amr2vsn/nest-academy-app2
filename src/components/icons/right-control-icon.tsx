import React from 'react';
import Svg, { Path } from 'react-native-svg';
import { IconType } from '../types';

export const RightControlIcon: React.FC<IconType> = ({
  height = 49,
  width = 49,
}) => {
  return (
    <Svg width={width} height={height} viewBox="0 0 49 49" fill="none">
      <Path
        d="M32.667 25.862L11.795 39.776a1.02 1.02 0 01-1.587-.85V10.073a1.021 1.021 0 011.587-.849l20.872 13.914v-12.93a2.042 2.042 0 014.083 0v28.584a2.042 2.042 0 11-4.083 0v-12.93zM14.292 15.796v17.407L27.346 24.5l-13.054-8.704z"
        fill="#FAFAFA"
      />
    </Svg>
  );
};
