import * as React from 'react';
import Svg, { Path } from 'react-native-svg';
import { IconType } from '../types';

export const BookOpenIcon: React.FC<IconType> = ({
  width = 18,
  height = 18,
}) => {
  return (
    <Svg width={width} height={height} viewBox="0 0 18 18" fill="none">
      <Path
        d="M9.833 15.5v1.667H8.166V15.5H1.5a.833.833 0 01-.833-.833V1.333A.833.833 0 011.5.5h5A3.325 3.325 0 019 1.628 3.325 3.325 0 0111.5.5h5a.833.833 0 01.833.833v13.334a.833.833 0 01-.833.833H9.833zm5.833-1.667V2.167H11.5a1.667 1.667 0 00-1.667 1.666v10h5.833zm-7.5 0v-10A1.667 1.667 0 006.5 2.167H2.333v11.666h5.833z"
        fill="#172B4D"
      />
    </Svg>
  );
};
