import React from 'react';
import Svg, { Path } from 'react-native-svg';
import { IconType } from '../types';

export const HeaderRightIcon: React.FC<IconType> = ({
  height = 32,
  width = 32,
}) => {
  return (
    <Svg width={width} height={height} viewBox="0 0 32 32" fill="none">
      <Path
        d="M16.085 14.115l6.565-6.6L24.525 9.4 17.96 16l6.565 6.6-1.875 1.885-6.565-6.6-6.565 6.6L7.645 22.6 14.21 16 7.644 9.4 9.52 7.515l6.565 6.6z"
        fill="#303030"
      />
    </Svg>
  );
};
