import React from 'react';
import { Border, Box, Spacing, Text } from '../../components';

const typeToBackgroundRole: any = {
  default: {
    subtle: 'primary200',
    bold: 'primary500',
  },
  success: {
    subtle: 'success200',
    bold: 'success400',
  },
  pending: {
    subtle: 'caution200',
    bold: 'caution400',
  },
  error: {
    subtle: 'destructive200',
    bold: 'destructive400',
  },
};

const typeToTextRole: any = {
  default: {
    subtle: 'primary500',
    bold: 'primary100',
  },
  success: {
    subtle: 'success500',
    bold: 'primary100',
  },
  pending: {
    subtle: 'caution500',
    bold: 'primary500',
  },
  error: {
    subtle: 'destructive500',
    bold: 'primary100',
  },
};

export const Lozenge: React.FC<LozengeType> = ({
  type = 'default',
  style = 'bold',
  children,
}) => {
  return (
    <Box height={24} width={'auto'} alignSelf={'flex-start'}>
      <Border radius={4}>
        <Box role={typeToBackgroundRole[type][style]}>
          <Spacing pv={1} ph={2}>
            <Text bold type={'caption1'} role={typeToTextRole[type][style]}>
              {children}
            </Text>
          </Spacing>
        </Box>
      </Border>
    </Box>
  );
};

type LozengeType = {
  type: 'default' | 'success' | 'pending' | 'error';
  style: 'bold' | 'subtle';
  children: string;
};

export const LozengeStatus: React.FC<LozengeStatusType> = ({
  type = 'default',
  style = 'bold',
  status,
  id,
}) => {
  return (
    <Box
      flexDirection={'row'}
      height={24}
      width={'100%'}
      justifyContent={'space-between'}
      alignItems={'center'}
    >
      <Lozenge type={type} style={style}>
        {status}
      </Lozenge>
      {id && (
        <Spacing ph={2}>
          <Text
            numberOfLines={1}
            width={'auto'}
            type={'subheading'}
            role={'black'}
            opacity={0.5}
          >
            {id}
          </Text>
        </Spacing>
      )}
    </Box>
  );
};

type LozengeStatusType = {
  type: 'default' | 'success' | 'pending' | 'error';
  style: 'bold' | 'subtle';
  status: string;
  id?: string;
};
