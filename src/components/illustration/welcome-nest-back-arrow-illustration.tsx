import React from 'react';
import Svg, { G, Path } from 'react-native-svg';

export const WelcomeNestBackArrowIllustration: React.FC<any> = (props) => {
  return (
    <Svg
      width={11}
      height={18}
      viewBox="0 0 11 18"
      fill="none"
      xmlns="http://www.w3.org/2000/svg"
      {...props}
    >
      <Path
        d="M4.36 9l6.565 6.6-1.875 1.885L.61 9 9.05.515 10.925 2.4 4.36 9z"
        fill="#303030"
      />
    </Svg>
  );
};
