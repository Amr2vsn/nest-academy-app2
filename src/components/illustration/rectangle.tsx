import React from 'react';
import Svg, { Path, G, Mask } from 'react-native-svg';

export const Rectangle = () => {
  return (
    <Svg width={95} height={220} viewBox="0 0 95 220" fill="none">
      <Mask id="prefix__a" x={10} y={41} width={177} height={276}>
        <Path
          opacity={1}
          fillRule="evenodd"
          clipRule="evenodd"
          d="M79.102 59.788l-50.221 87.311 89.049 50.718 50.221-87.312-89.049-50.717zm1.895-17.334c-4.319-2.46-9.83-.961-12.308 3.347L11.487 145.25a8.953 8.953 0 003.333 12.255l101.215 57.646c4.319 2.46 9.829.962 12.308-3.347l57.202-99.447a8.954 8.954 0 00-3.334-12.256L80.997 42.454z"
          fill="#007AFF"
        />
      </Mask>
      <G mask="url(#prefix__a)">
        <Path
          fillRule="evenodd"
          clipRule="evenodd"
          d="M6 26v213h2V26H6zm5 0v213h2V26h-2zm5 213V26h2v213h-2zm5-213v213h2V26h-2zm5 213V26h2v213h-2zm5-213v213h2V26h-2zm5 213V26h2v213h-2zm5-213v213h2V26h-2zm5 213V26h2v213h-2zm5-213v213h2V26h-2zm5 213V26h2v213h-2zm5-213v213h2V26h-2zm5 213V26h2v213h-2zm5-213v213h2V26h-2zm5 213V26h2v213h-2zm5-213v213h2V26h-2zm5 213V26h2v213h-2zm5-213v213h2V26h-2zm5 213V26h2v213h-2zm5-213v213h2V26h-2zm5 213V26h2v213h-2zm5-213v213h2V26h-2zm5 213V26h2v213h-2zm5-213v213h2V26h-2zm5 213V26h2v213h-2zm5-213v213h2V26h-2zm5 213V26h2v213h-2zm5-213v213h2V26h-2zm5 213V26h2v213h-2zm5-213v213h2V26h-2zm5 213V26h2v213h-2zm5-213v213h2V26h-2zm5 213V26h2v213h-2zm5-213v213h2V26h-2zm5 213V26h2v213h-2zm5-213v213h2V26h-2zm5 213V26h2v213h-2zm5-213v213h2V26h-2z"
          fill="#7DBAFC"
        />
      </G>
      <Path
        opacity={0.5}
        fillRule="evenodd"
        clipRule="evenodd"
        d="M72.102 21.788l-50.221 87.311 89.049 50.718 50.221-87.312-89.049-50.717zm1.895-17.334c-4.319-2.46-9.83-.961-12.308 3.347L4.487 107.25a8.953 8.953 0 003.334 12.255l101.214 57.646c4.319 2.46 9.829.962 12.308-3.347l57.202-99.447a8.954 8.954 0 00-3.334-12.256L73.997 4.454z"
        fill="#007AFF"
      />
    </Svg>
  );
};
