import React, { useEffect, useRef, useState } from 'react';
import { createMaterialTopTabNavigator } from '@react-navigation/material-top-tabs';
import {
  TouchableOpacity,
  Dimensions,
  SafeAreaView,
  View,
  Animated,
  ScrollView,
} from 'react-native';
import { Box, Text } from '../components';
import {
  FirstScreen,
  SecondScreen,
  ThirdScreen,
} from '../screen/test/test-screen';
import {
  AboutScreen,
  ProgramScreen,
  ProjectScreen,
} from '../screen/main-cources';

const { width } = Dimensions.get('window');
const Tab = createMaterialTopTabNavigator();

export const MyTab: React.FC<any> = ({
  state,
  descriptors,
  navigation,
  position,
}) => {
  const xVal = useRef(new Animated.Value(0)).current;

  useEffect(() => {
    Animated.spring(xVal, {
      toValue: state.index,
      velocity: 0.1,
      useNativeDriver: true,
    }).start();
  }, [state.index]);

  return (
    <SafeAreaView>
      {/* ene n bolohoor yg deed taliin topTab n yvj bgn */}
      <Box height={49} width={'100%'} flexDirection="row">
        {state.routes.map((route: any, index: any) => {
          const { options } = descriptors[route.key];
          const label =
            options.tabBarLabel !== undefined
              ? options.tabBarLabel
              : options.title !== undefined
              ? options.title
              : route.name;
          const isFocused = state.index === index;

          const onPress = () => {
            const event = navigation.emit({
              type: 'tabPress',
              target: route.key,
              canPreventDefault: true,
            });
            if (!isFocused && !event.defaultPrevented) {
              navigation.navigate(route.name);
            }
          };

          return (
            <TouchableOpacity
              accessibilityRole="button"
              accessibilityState={isFocused ? { selected: true } : {}}
              accessibilityLabel={options.tabBarAccessibilityLabel}
              testID={options.tabBarTestID}
              onPress={onPress}
              style={{
                flex: 1,
                alignItems: 'center',
                justifyContent: 'center',
              }}
              key={route.name}
            >
              <Text
                bold={isFocused ? true : false}
                textAlign="center"
                fontFamily="Montserrat"
              >
                {label}
              </Text>
            </TouchableOpacity>
          );
        })}
      </Box>

      {/* dood taliin guudeg animation */}
      <Box height={2} width="100%">
        <Box height={2} width="100%" role="gray" position={'absolute'} />
        <Animated.View
          style={[
            {
              height: 2,
              width: width / state.routes.length,
              backgroundColor: '#172B4D',
              position: 'absolute',
            },
            {
              transform: [
                {
                  translateX: Animated.multiply(
                    width / state.routes.length,
                    xVal
                  ),
                },
              ],
            },
          ]}
        />
      </Box>
    </SafeAreaView>
  );
};

// yg end main n eheljiga bhguyu
export const MainTopNav = () => {
  return (
    <Tab.Navigator style={{}} tabBar={(props) => <MyTab {...props} />}>
      <Tab.Screen name="First" component={FirstScreen} />
      <Tab.Screen name="Second" component={SecondScreen} />
      <Tab.Screen name="Third" component={ThirdScreen} />
    </Tab.Navigator>
  );
};

export const CourcesTab = () => {
  return (
    <Tab.Navigator tabBar={(props) => <MyTab {...props} />}>
      <Tab.Screen name="Тухай" component={AboutScreen} />
      <Tab.Screen name="Хөтөлбөр" component={ProgramScreen} />
      <Tab.Screen name="Бүтээлүүд" component={ProjectScreen} />
    </Tab.Navigator>
  );
};
