import React from 'react';
import {
  Spacing,
  Button,
  Text,
  HeaderLeftIcon,
  HeaderRightIcon,
} from '../components';

export const HeaderLeft: React.FC<any> = ({ onPress }) => (
  <Button onPress={onPress} category="text">
    <Spacing ml={2}>
      <HeaderLeftIcon />
    </Spacing>
  </Button>
);

export const HeaderTitle: React.FC<any> = ({ title = 'test' }) => (
  <Text
    fontFamily={'Montserrat'}
    type={'headline'}
    role={'black'}
    bold
    width={'auto'}
  >
    {title}
  </Text>
);

export const HeaderRight: React.FC<any> = ({ onPress }) => (
  <Button onPress={onPress} category="text">
    <Spacing mr={2}>
      <HeaderRightIcon />
    </Spacing>
  </Button>
);
