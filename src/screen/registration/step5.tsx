import React, { useContext, useEffect } from 'react';
import { Dimensions } from 'react-native';
import { Box, ProgressStepsContext, Spacing, Stack, Text } from '../../components';
import { CourseCard } from './courseCard';

const { width } = Dimensions.get('window');

export const Step5 = () => {
  const { userInfo, setUserInfo } = useContext(ProgressStepsContext);
  const currentYear = new Date().getFullYear();
  const age = currentYear - Number(userInfo.date.slice(0, 4));

  useEffect(() => {
    setUserInfo((userInfo: any) => ({
      ...userInfo,
      course: age < 18 ? 'hop' : 'leap',
    }));
  }, []);

  return (
    <Box flex={1} alignItems="center">
      <Spacing mt={13} mb={18} ph={4}>
        <Stack size={2} alignItems={'center'}>
          <Text type={'headline'} bold textAlign={'center'}>
            Танд санал болгож буй хөтөлбөр
          </Text>
          <Text
            width={343}
            role={'primary400'}
            type={'body'}
            textAlign={'center'}
          >
            Таны нас манай HOP хөтөлбөрт тохирч байгаа бөгөөд та дэлгэрэнгүй
            мэдээллийг сонирхох боломжтой.
          </Text>
        </Stack>
      </Spacing>

      {/* <Spacing ph={4}> */}
        {/* <Box flex={1} height={2} role={'accentNest'}> */}
          <CourseCard course={age < 18 ? 'hop' : 'leap'} />
        {/* </Box> */}
      {/* </Spacing> */}
    </Box>
  );
};
