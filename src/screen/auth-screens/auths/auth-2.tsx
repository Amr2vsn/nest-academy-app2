import React, { useState, useContext } from 'react';
import { Box, Text, DigitInput, Spacing, CountdownTimer, Stack, Button } from '../../../components';
import { Dimensions } from 'react-native';
import { AuthContext } from '../../../provider/auth';

export const Auth2 = ({ error }: any) => {
  const [resend, setResend] = useState(false);
  const { resendCode } = useContext(AuthContext);
  return (
    <Box flex={1} role={'white'}>
      <Spacing mt={5} mh={4} grow={1}>
        <Box flex={1} justifyContent={'space-between'}>
          <Stack size={12} alignItems={'center'}>
            <Text height={44} bold textAlign="center" width={240} type="headline">
              Таны дугаарт илгээсэн 6 оронтой кодыг оруулна уу
            </Text>
            <Box height={56} justifyContent="center" alignItems="center">
              <DigitInput />
            </Box>
          </Stack>

          <Box>
            {error &&
              <Box width={'100%'} flexDirection={'row'} justifyContent={'center'}>
                <Button onPress={() => { resendCode(); setResend(false) }} category='text'>
                  <Text underline role={'destructive500'} type={'subheading'}>
                    Алдаа дахин шинэ код оруулна уу
                  </Text>
                </Button>
              </Box>
            }
            {!error &&
              <Spacing mb={4}>
                <Box width={'100%'} flexDirection={'row'} justifyContent={'center'}>
                  <Button onPress={() => { resendCode(); setResend(false) }} status={resend ? 'active' : 'disabled'} category='text'>
                    {
                      resend ? 
                        <Text type={'subheading'} role={'primary500'}>Дахин код авах</Text>:
                        <Box flexDirection={'row'}>
                          <Spacing mr={2}>
                            <Text type={'subheading'} role={'darkgray'}>
                              Дахин код авах
                            </Text>
                          </Spacing>
                          <CountdownTimer type={'subheading'} role={'darkgray'} from={30} onFinish={() => setResend(true)}></CountdownTimer>
                        </Box>
                    }
                  </Button>
                </Box>
              </Spacing>}
          </Box>
        </Box>
      </Spacing>
    </Box>
  );
};
