import { useNavigation } from '@react-navigation/core';
import React, { useContext, useEffect, useState } from 'react';
import { Box, ProgressStep, ProgressSteps } from '../../components/';
import { NavigationRoutes } from '../../navigation/navigation-params';
import { AuthContext } from '../../provider/auth';
import { Auth1, Auth2 } from './auths/index';

export const Login = () => {
  const navigation = useNavigation();
  const [error, setError] = useState(null);
  const { signInWithPhoneNumber, confirmCode, setcode } = useContext(AuthContext);
  const section1 = async () => {
    await signInWithPhoneNumber();
  };

  useEffect(() => {
    setcode(null);
  }, [])
  
  const section2 = async () => {
    try {
      await confirmCode();
      await navigation.navigate(NavigationRoutes.MainRoot);
    } catch(e) {
      setError(e.code)
    }
  };

  return (
    <Box flex={1} role={'white'}>
      <ProgressSteps
        lastButtonFunction={() => {
          section2();
        }}
      >
        <ProgressStep
          label="Login1"
          id="Login1"
          onNext={() => {
            section1();
          }}
        >
          <Auth1 title="Гар утасны дугаараа оруулна уу" />
        </ProgressStep>
        <ProgressStep label="Login2" id="Login2">
          <Auth2 error={error} />
        </ProgressStep>
      </ProgressSteps>
    </Box>
  );
};
