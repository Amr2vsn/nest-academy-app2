import React from 'react';
import { Image } from 'react-native';
import { ScrollView } from 'react-native-gesture-handler';
import { SafeAreaView } from 'react-native-safe-area-context';
import { Box, Button, Spacing, Text } from '../../components';
import { useQuery } from '@apollo/client';
import { REQUEST_NEWS } from '../../graphql/queries';

const foundation = require('../../assets/images/foundation-image.png');
const exampleImg = require('../../assets/images/image1.png');

export const News = () => {
  // const { data, loading, error } = useQuery(REQUEST_NEWS)

  return (
    <ScrollView>
      <Box height={'auto'}>
        <Image
          style={{ width: '100%' }}
          resizeMode={'stretch'}
          source={foundation}
        />
        <Spacing mt={3} />
        <Box>
          <Spacing p={3}>
            {/* Date */}
            <Text type={'subheading'} bold>
              2020 / 03 / 19
            </Text>
            <Spacing mt={2} />
            <Text bold type={'title3'}>
              Demo Day 2020 арга хэмжээ амжилттай болж өнгөрлөө.
            </Text>
            <Spacing mt={3} />
            <Text>
              Дэлхийн хамгийн том сошиал платформыг ажиллуулдаг Фэйсбүүк компани
              Twitter, Clubhouse зэрэг бүтээгдэхүүнүүдтэй өрсөлдөх зорилгоор
              аудио үйлчилгээнүүд нэвтрүүлэх гэж байгаагаа өчигдөр зарлажээ.
            </Text>
            <Spacing mt={5} />
            <Text>
              Өчигдөр хэвлэлд ярилцлага өгөх үеэрээ Фэйсбүүкийн гүйцэтгэх
              захирал Марк Цукербэрг энэ тухай анх мэдээлсэн байна. Энэ оны
              зунаас зах зээлд нэвтрэх шинэ үйлчилгээнүүд нь богино аудио клип
              илгээх “Soundbites”, аудио хэлэлцүүлэг өрнүүлдэг “Live audio”
              өрөөнүүд байх гэнэ.
            </Text>
          </Spacing>
        </Box>
        <Image
          style={{ width: '100%', height: 250 }}
          resizeMode={'contain'}
          source={exampleImg}
        />
        <Box>
          <Spacing p={3}>
            <Text>
              Ingenuity нэртэй жижиг дрон ердөө нэг минут хүрэхгүй хугацаанд
              нисээд амжилттай газардсан байна. Гэхдээ энэ нь хүн төрөлхтөн
              дэлхийгээс өөр гарагийн тэнгэрт өөрийн бүтээсэн техникийг нисгэсэн
              анхны тохиолдол тул түүхэнд бичигдэх үйл явдал юм.
            </Text>
            <Spacing mt={5} />
            <Text>
              Ангараг гарагийн тойрог замд байгаа хиймэл дагуул дроны төлөвийг
              дэлхий рүү дамжуулснаар туршилт амжилттай болсныг мэдсэн байна.
            </Text>
            <Spacing mt={7} />
            <Button width={'100%'} category={'ghost'} onPress={() => {}}>
              <Text bold fontFamily="Montserrat">
                Бичлэг үзэх
              </Text>
            </Button>
          </Spacing>
        </Box>
      </Box>
    </ScrollView>
  );
};
