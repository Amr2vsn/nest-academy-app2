import React, { useEffect, useState } from 'react';
import {
  ChangeAvatarImage,
  Box,
  Queue,
  Spacing,
  Stack,
} from '../../components/layout';
import { Text } from '../../components/core/text';
import { Button } from '../../components/core/button';
import { PersonalInformationGender } from './personal-informatio-gender';
import { ScrollView } from 'react-native-gesture-handler';
import { Input, MultilineInput } from '../../components';
import { useDocument } from '../../hooks';
import auth from '@react-native-firebase/auth';
import { useSafeAreaInsets } from 'react-native-safe-area-context';

export const PersonalInformationLeap: React.FC<PersonalInformationLeapType> = () => {
  const [state, setState] = useState({
    lastName: 'lastName',
    firstName: 'firstName',
    age: 'age',
    edu: 'EБС',
    job:
      'Хоногт бүртгэгдэх тохиолдлын тоо 100, 200-аар яригддаг байсан бол өчигдөр Улаанбаатар хотод 731 тохиолдол бүртгэгдлээ.',
    number: 'number',
    number1: 'number1',
    mail: 'mail',
  });
  const user: any = auth().currentUser;
  const userData: any = useDocument(`users/${user.uid}`);
  const insets = useSafeAreaInsets();

  useEffect(() => {
    if (userData.doc != null) {
      setState((state) => ({ ...state, lastName: userData.doc.lastname }));
      setState((state) => ({ ...state, firstName: userData.doc.firstname }));
      setState((state) => ({ ...state, age: userData.doc.age }));
      setState((state) => ({ ...state, edu: userData.doc.edu }));
      setState((state) => ({ ...state, number: userData.doc.number }));
      setState((state) => ({ ...state, number1: userData.doc.number1 }));
      setState((state) => ({ ...state, mail: userData.doc.mail }));
    }
  }, [userData.doc]);

  return (
    <ScrollView>
      <Spacing p={4}>
        <Spacing m={6}>
          <ChangeAvatarImage size="L" />
        </Spacing>
        <Stack size={3} alignItems={'center'}>
          <Input
            placeholder={'Овог'}
            value={state.lastName}
            onChangeText={(text: any) =>
              setState((state) => ({ ...state, lastName: text }))
            }
          ></Input>
          <Input
            placeholder={'Нэр'}
            value={state.firstName}
            onChangeText={(text: any) =>
              setState((state) => ({ ...state, firstName: text }))
            }
          ></Input>
          <Input
            placeholder={'Нас'}
            value={state.age}
            onChangeText={(text: any) =>
              setState((state) => ({ ...state, age: text }))
            }
          ></Input>
          <Input
            placeholder={'Боловсролын байдал'}
            value={state.edu}
            onChangeText={(text: any) =>
              setState((state) => ({ ...state, edu: text }))
            }
          ></Input>
          <MultilineInput
            placeholder={'Эрхэлж буй ажил, сургуулийн товч танилцуулга'}
            value={state.job}
            multiline
          />
          <Spacing mt={1}>
            <Queue size={2}>
              <PersonalInformationGender desc={'Эрэгтэй'} type={'male'} isSelected />
              <PersonalInformationGender desc={'Эмэгтэй'} type={'female'} />
              <PersonalInformationGender desc={'Бусад'} type={'neutral'} />
            </Queue>
          </Spacing>
          <Spacing mt={3} mb={2}>
            <Box role={'primary200'} height={1} width={342} />
          </Spacing>
          <Spacing mb={2}>
            <Text type={'headline'} fontFamily={'Montserrat'} width={342} textAlign='left' bold>Холбогдох мэдээлэл</Text>
          </Spacing>
          <Input
            placeholder={'Утас'}
            value={state.number}
            onChangeText={(text: any) =>
              setState((state) => ({ ...state, number: text }))
            }
          ></Input>
          <Input
            placeholder={'Яааралтай үед холбоо барих'}
            value={state.number1}
            onChangeText={(text: any) =>
              setState((state) => ({ ...state, number1: text }))
            }
          ></Input>
          <Input
            placeholder={'И-мейл'}
            value={state.mail}
            onChangeText={(text: any) =>
              setState((state) => ({ ...state, mail: text }))
            }
          ></Input>
          <Spacing mt={3} mb={4 + insets.bottom / 4}>
            <Button
              size={'l'}
              width={342}
              onPress={() => userData.updateRecord(state)}
            >
              <Text
                role={'white'}
                bold
                fontFamily={'Montserrat'}
                type={'callout'}
              >
                Хадгалах
              </Text>
            </Button>
          </Spacing>
        </Stack>
      </Spacing>
    </ScrollView>
  );
};

type PersonalInformationLeapType = {};
