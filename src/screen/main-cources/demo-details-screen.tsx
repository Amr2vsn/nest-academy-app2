import React from 'react';
import { FlatList, SafeAreaView, ScrollView } from 'react-native';
import { VoluData } from './course-data';
import {
  BackgroundImage,
  Box,
  Comment,
  Spacing,
  Text,
  Button,
} from '../../components';
import { PhotoCard } from './photo-card';
import { useQuery } from '@apollo/client';
import { Request_DemoProjects } from '../../graphql/queries'

export const DemoDetailsScreen: React.FC<any> = ({ route, navigation }) => {
  const { data, error, loading } = useQuery(Request_DemoProjects);
  data && console.log('data:' + data)
  const {
    source,
    source2,
    introduction,
    students,
    comments,
  } = VoluData;
  return (
    <SafeAreaView>
      <ScrollView>
        <BackgroundImage source={source} width='100%' height={375} />
        <Box>
          <Spacing mt={6} mb={4} mh={5}>
            <Text type="body" role="primary500" bold>
              Танилцуулга
            </Text>
          </Spacing>
          <Spacing mh={5}>
            <Text role="black" type="body">
              {introduction}
            </Text>
          </Spacing>
        </Box>
        <BackgroundImage source={source2} width='100%'/>
        <Spacing mt={10} mb={6} mh={5}>
          <Text role="primary500" type="body" bold>
            Сурагчид
          </Text>
        </Spacing>
        <Spacing ml={5}>
          <FlatList
            data={students}
            horizontal
            showsHorizontalScrollIndicator={false}
            ItemSeparatorComponent={() => <Box width={16} />}
            renderItem={({ item }) => (
              <PhotoCard
                name={item.name}
                title={item.title}
                source={item.source}
                icon={item.icon}
                width={264}
                height={320}
              />
            )}
          />
        </Spacing>
        <Spacing mt={6} mb={6} ml={5}>
          <Text role="primary500" type="body" bold>
            Сэтгэгдэл
          </Text>
        </Spacing>
        <Spacing mb={15} ml={5}>
          <FlatList
            data={comments}
            horizontal
            showsHorizontalScrollIndicator={false}
            renderItem={({ item }) => (
              <Comment
                name={item.name}
                title={item.title}
                description={item.description}
                source={item.source}
                width={311}
              />
            )}
          />
        </Spacing>
      </ScrollView>
      <Box bottom={30} position={'absolute'} width={'90%'} alignSelf={'center'}>
        <Button
          size="l"
          width={'100%'}
          onPress={() => navigation.navigate(NavigationRoutes.Registration)}
        >
          <Text fontFamily={'Montserrat'} type={'callout'} role={'white'} bold>
            ТУРШИЖ ҮЗЭХ
          </Text>
        </Button>
      </Box>
    </SafeAreaView>
  );
};

interface students {
  id: number;
  name: string;
  title: string;
  icon: any;
  source: any;
}
interface comments {
  id: number;
  name: string;
  title: string;
  description: string;
  source: any;
}

interface DemoDetails {
  name: string;
  source: any;
  source2: any;
  introduction: string;
  students: Array<students>;
  comments: Array<comments>;
}
